﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterMovement : MonoBehaviour
{

    public float speed; //speed at which monsters will approach (defined in unity inspector)
 
    private bool canWalk = true; //can monsters walk?
  
    //components references
    Rigidbody rb;
    Animator anim;
    

    public GameObject scarer;
    public Transform player;



    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
      
        

        scarer = GameObject.FindGameObjectWithTag("StopArea");
    }

    void Start()
    {
       
       
        
    }

    void Update()
    {
        Rotate();
        Move();
    }

    //// Update is called once per frame
    //void FixedUpdate()
    //{
    //    Move();
    //}
    
    void Move()
    {
       
       if (canWalk)
       {
           
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, player.position, step);
            anim.SetBool("walk", true);
       }
       
    }

    void Rotate() //rotate towards player
    {
        Vector3 rotationTowardsTarget = player.position - transform.position;

        rotationTowardsTarget.y = 0f;

        Quaternion rotation = Quaternion.LookRotation(rotationTowardsTarget);
        rb.MoveRotation(rotation);
    }

    void OnTriggerEnter(Collider col)
    {
        //if enemies entered certain area
        if (col.CompareTag("StopArea"))
        {
            canWalk = false;
            anim.SetBool("Scare", true);
           
         
            scarer.GetComponent<ScareFactor>().AddToList(this.gameObject);
        }

   
    }

    void OnTriggerExit(Collider col)
    {
        //if enemies entered certain area
        if (col.CompareTag("StopArea"))
        {
            canWalk = false;
            anim.SetBool("Scare", true);

          
            
        }
    }


}
