﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterScript : MonoBehaviour {

    
    private float health = 100;
    private ScareFactor scareScript;


    private GameObject waveManager;
    public float Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
        }
    }
    private void Awake()
    {
        //scareScript = GetComponent<ScareFactor>();
    }
    // Use this for initialization
    void Start () {
        waveManager = GameObject.FindWithTag("WaveManager");
        scareScript = GameObject.FindWithTag("StopArea").GetComponent<ScareFactor>();

        Debug.Log(health);
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(health);
    }

    public void TakeDamage(float amount) {

        health -= amount;
        if(health <= 0)
        {
            Kill();
        }
          
    }


    void Kill()
    { 
        Debug.Log("Removed from list");

        //spawn some particle fx

        Destroy(gameObject);
     
        scareScript.GetComponent<ScareFactor>().RemoveFromList();

    }
}
