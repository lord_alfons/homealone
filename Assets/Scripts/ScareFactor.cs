﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScareFactor : MonoBehaviour {

    //create a slider and attach it
    //change max scare percentage to any value you'd like and set the same value as 'max' in slider
    //set the startTimer to any value you'd like (how fast should the kid be scared?)

    public List<GameObject> monsters = new List<GameObject>();
  
    public Slider scareSlider;

    private int percentage;
    public int maxpercentage;

    private float timer;
    public float startTimer;

    public GameController cont;

    void Start()
    {
        percentage = 0;
   
    }

    void Update()
    {
        Debug.Log("MONSTERS" + monsters.Count);

        if (monsters.Count > 0)
        {
            foreach (GameObject go in monsters)
            {
                if (timer <= 0)
                {
                    percentage += 5;
                    timer = startTimer;
                }

                else timer -=  Time.deltaTime;

                if(percentage > maxpercentage)
                {
                    percentage = maxpercentage;
                }

            }
        }

        else
        {
             
               //reduce the amount if no enemies in range
               if (timer <= 0)
               {
                   percentage -= 1;
                   timer = startTimer;
               
               }
               
               else timer -= Time.deltaTime;
               
               if (percentage < 0)
               {
                   percentage = 0;
               }
          
        }

        scareSlider.value = percentage;

 
        //lose condition (if percentage == maxpercentage), then call end of game function
        if (percentage >= maxpercentage)
        {
            StartCoroutine(cont.GetComponent<GameController>().EndOfGame());
        }
    }

    
    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Monster"))
        {
           // RemoveFromList(col.gameObject);
            
        }
    }

    public void AddToList(GameObject other)
    {
        monsters.Add(other);
    }

    public void RemoveFromList()
    {
        // since this list is only  used for counting monsters  we can remove at 0
        if (monsters.Count>0) // checks if list is empty if not it can remove from 0
        {
            monsters.RemoveAt(0);
        }


    }

}
