﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawnerScript : MonoBehaviour
{

    public Transform[] spawnPoints = new Transform[8];
    public GameObject[] enemies = new GameObject[8];
    

    public float timer;
    public float minTimer;

    public float minDistance;

    void Start()
    {
        Invoke("SpawnEnemyRandom", timer);
    }

    void SpawnEnemyRandom()
    {
        int random = Random.Range(0, spawnPoints.Length);
        Transform sp = spawnPoints[random];
        GameObject go = enemies[random];


        if (Vector3.Distance(sp.position, go.transform.position) <= minDistance)
            Invoke("SpawnEnemyRandom", 0.1f);

        else Instantiate(go, sp.position, Quaternion.identity);

        timer -= 0.1f;

        if (timer < minTimer) timer = minTimer;

        Invoke("SpawnEnemyRandom", timer);
    }
 

}
